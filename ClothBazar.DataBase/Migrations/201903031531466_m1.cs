namespace ClothBazar.DataBase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Name = c.String(),
                        Status = c.Boolean(nullable: false),
                        Categorys_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.Categorys_ID)
                .Index(t => t.Categorys_ID);
            
            AddColumn("dbo.Categories", "Name", c => c.String());
            AddColumn("dbo.Categories", "Status", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Categorys_ID", "dbo.Categories");
            DropIndex("dbo.Products", new[] { "Categorys_ID" });
            DropColumn("dbo.Categories", "Status");
            DropColumn("dbo.Categories", "Name");
            DropTable("dbo.Products");
        }
    }
}
