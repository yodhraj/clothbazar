﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClothBazar.Entity;

namespace ClothBazar.DataBase
{
    public class CbDbContext : DbContext
    {
        public CbDbContext():base("DefaultConnection1")
        {

        }
        public DbSet<Category> Categorys { get; set; }
        public DbSet<Product> Products { get; set; }

    }

     
}
