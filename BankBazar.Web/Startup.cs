﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BankBazar.Web.Startup))]
namespace BankBazar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
